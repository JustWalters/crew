Template.profilePage.helpers({
	crewName: function() {
		var crewId = Meteor.user().profile.crews;
		var theCrew = Crews.findOne(crewId);
		return theCrew.name ? theCrew.name : "We don't know";//obviously temp TODO*/
	},
	self: function() {
		return this._id === Meteor.user()._id;
	},
	beingPursued: function() {
		return Meteor.user().profile.suitors;
	}
});

Template.profilePage.events({
  'submit .add': function(e) {
    e.preventDefault();

    Meteor.call("addMember", Meteor.user()._id, this._id, function(error, id){
			if (error) return alert(error.reason);
		});
    Router.go("myProfile");
    //TODO add callback that tells user what happened
  },
  "click #yes": function(e) {
  	console.log("yes");	
  },
  "click #no": function(e) {
  	console.log("no");
  }
});