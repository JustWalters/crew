var memberData = [
{
	userId: 1234,
	username: "j-wall"
},
{
	userId: 1235,
	username: "roadtrax"
},
{
	userId: 1236,
	username: "lil saak"
}
];


Template.crewPage.helpers({
	members: function(){
		var crewId = Meteor.user().profile.crews;
		var theCrew = Crews.findOne(crewId);
		var Ids = theCrew.members;

		var len = Ids.length;
		var res = [];
		for (var i = 0; i < len; i++){
			var mem = Meteor.users.findOne(Ids[i]);
			res.push(mem.profile.pseudonym ? mem.profile.pseudonym : mem.profile.name);
		}

		return res;

	},
	crewPosts: function() {
		var crewId = Meteor.user().profile.crews;
		var theCrew = Crews.findOne(crewId);
		var Ids = theCrew.tracks;

		var len = Ids.length;
		var res = [];
		for (var i = 0; i < len; i++){
			var mem = Posts.findOne(Ids[i]);
			res.push(mem);
		}

		return res;
		//TODO return it in the proper way
	},
	/*myCrews: function() {
		//TODO make it work
		var mine = Meteor.user().profile.crews;
		var res = "";
		mine.forEach(function() {
			res += this.name;
		});
		return res;
	},*/
	whoami: function() {		
		return Meteor.user().profile.pseudonym ? 
		Meteor.user().profile.pseudonym :
		Meteor.user().profile.name;
	},
	crewname: function() {
		var crewId = Meteor.user().profile.crews;
		var theCrew = Crews.findOne(crewId);
		return theCrew.name ? theCrew.name : "We don't know";//obviously temp TODO*/
	},
	inCrew: function() {
		return Meteor.user().profile.crews ? true : false;
	}
});

//TODO make sure there's something there
Template.crewPage.events({
	"submit .new": function(e) {
		e.preventDefault();

		var newCrew = {
			name: $(e.target).find("[name=cname]").val(),
			members: [Meteor.user()._id],
			admins: [Meteor.user()._id],
			potentials: [],
			tracks: []
		};

		Meteor.call("newCrew", newCrew, Meteor.user()._id, function(error, id){
			if (error) return alert(error.reason);
		});
	},
	"submit .delete": function(e){
		//make sure
		e.preventDefault();

		var crewId = Meteor.user().profile.crews;

		Meteor.call("delCrew", crewId, Meteor.user()._id, function(error, id) {
			if (error) return alert(error.reason);

		});
	},
	"submit .add": function(e){
		e.preventDefault();
		//make sure user isn't already in a crew

		//get entered user's id, store in newId

	},
	"submit .remove": function(e){e.preventDefault();}
});