Template.notifications.helpers({
  notifications: function() {
    return Notifications.find({userId: Meteor.userId(), read: false});
  },
  notificationCount: function(){
  	return Notifications.find({userId: Meteor.userId(), read: false}).count();
  }
});

Template.notification.helpers({
  notificationPostPath: function() {
    if (this.commenterName !== null) {
      return Router.routes.postPage.path({_id: this.postId});
    } else if (this.adderName !== null) {
      return Router.routes.crewPage.path({_id: this.crewId});
    }
  },
  notificationDisplay: function() {
    var res = "";
    if (this.commenterName !== null) {
      res = "<strong>" + this.commenterName + "</strong> commented on your post";
      return new Handlebars.SafeString(res);
    } else if (this.adderName !== null) {
      res = "<strong>" + this.adderName + "</strong> wants you in their crew";
      return new Handlebars.SafeString(res);
    } else console.log("Wait! What's the notification?!")
  }
});

Template.notification.events({
  "click a": function() {
    Notifications.update(this._id, {$set: {read: true}});
  }
});