Router.configure({
	layoutTemplate: "layout",
	loadingTemplate: "loading",
	notFoundTemplate: "notFound",
	waitOn: function() {
		return [Meteor.subscribe("notifications")];
	}
});

PostsListController = RouteController.extend({
	template: "postsList",
	increment: 5,
	limit: function() {
		return parseInt(this.params.postsLimit) || this.increment;
	},
	findOptions: function() {
		return {sort: this.sort, limit: this.limit()};
	},
	waitOn: function() {
		return Meteor.subscribe("posts", this.findOptions());
	},
	posts: function() {
		return Posts.find({}, this.findOptions());
	},
	data: function() {
		var hasMore = this.posts().count() === this.limit();
		return {
			posts: this.posts(),
			nextPath: hasMore ? this.nextPath() : null
		};
	}
});

NewPostsListController = PostsListController.extend({
	sort: {submitted: -1, _id: -1},
	nextPath: function() {
		return Router.routes.newPosts.path({postsLimit: this.limit() + this.increment});
	}
});

BestPostsListController = PostsListController.extend({
	sort: {votes: -1, submitted: -1, _id: -1},
	nextPath: function() {
		return Router.routes.bestPosts.path({postsLimit: this.limit() + this.increment});
	}
});

Router.map(function() {
	this.route("home", {
		path: "/",
		controller: NewPostsListController
	});

	this.route("newPosts", {
		path: "/new/:postsLimit?",
		controller: NewPostsListController
	});

	this.route("bestPosts", {
		path: "/best/:postsLimit?",
		controller: BestPostsListController
	});

	this.route("postPage", {
		path: "/posts/:_id",
		waitOn: function() {
			return [
			Meteor.subscribe("singlePost", this.params._id),
			Meteor.subscribe("comments", this.params._id)
			];
		},
		data: function() { return Posts.findOne(this.params._id); }
	});

	this.route("postEdit", {
		path: "/posts/:_id/edit",
		waitOn: function() {
			return Meteor.subscribe("singlePost", this.params._id);
		},
		data: function() { return Posts.findOne(this.params._id); }
	});

	this.route("postSubmit", {
		path: "/submit",
		progress: {enabled: false}
	});

	this.route("myCrew", {
		path: "/myCrew",
		template: "crewPage",
		waitOn:  function() {
			return [
			Meteor.subscribe("crews"), 
			Meteor.subscribe("crewMembers"), 
			Meteor.subscribe("posts", {})
			];
		}
	});

	this.route("crewPage", {
		path: "/crew/:_id",
		waitOn: function() {
			return [
			Meteor.subscribe("crews"), 
			Meteor.subscribe("crewMembers"), 
			Meteor.subscribe("posts", {})
			];
		},
		data: function() {
      	return Crews.findOne(this.params._id);//TODO fix
   	}
	});

	this.route("myProfile", {
		path: "/me",
		template: "profilePage",
		waitOn: function() {
			return Meteor.subscribe("crews");
		},
		data: function() {
			return Meteor.users.findOne(Meteor.user()._id);
		}
	});

	this.route("profilePage", {
		path: "/profile/:_id",
		waitOn: function() {
			return [Meteor.subscribe("singleUser", this.params._id),
			Meteor.subscribe("postsOfUser", this.params._id),
			Meteor.subscribe("crews", this.params._id)];
		},
		data: function() {
			return Meteor.users.findOne(this.params._id);
		}		
	});
});


var requireLogin = function(pause) {
	if (! Meteor.user()) {
		if (Meteor.loggingIn())
			this.render(this.loadingTemplate);
		else
			this.render("accessDenied");

		pause();
	}
};

Router.onBeforeAction("loading");
Router.onBeforeAction(requireLogin, {only: ["postSubmit","crewPage"]});
Router.onBeforeAction(function() { clearErrors(); });