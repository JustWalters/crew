Crews = new Meteor.Collection("crews");

Crews.allow({
	insert: function(userId, doc){
		return !! userId;
	}
	//TODO add depending on settings, remove by admin
});

Meteor.methods({
	addMember: function(adderId, addedId) {
		createAddedToCrewNotification(adderId, addedId);

		var crewId = Meteor.users.findOne(adderId).profile.crews;
		var crew = Crews.findOne(crewId);
		if (crew.potentials) crew.potentials.push(addedId);
		else crew.potentials = [addedId];

		var potential = Meteor.users.findOne(addedId);
		if (potential.profile.suitors) potential.profile.suitors.push(crewId);
		else potential.profile.suitors = [crewId];
		//TODO this is inadequate
	},
	removeMember: function(userId) {},
	newCrew: function(crew, userId) {
		/*Crews.find({}).forEach(function(post) {
			console.log("Crewb4new = " + post);
		});
		Meteor.users.find({}).forEach(function(post) {
			console.log("Userb4new = " + post);
		});*/

		var crewId = Crews.insert(crew, function(error, id) {
			console.log("error " + error);
			console.log(id);
		});
		Meteor.users.update(userId, {$set:{"profile.crews":crewId}}, function(error, updated) {
			//TODO make it better
			console.log(error);
			console.log(updated);
		});

		/*Crews.find({}).forEach(function(post) {
			console.log("Crew@new = " +post);
		});
		Meteor.users.find({}).forEach(function(post) {
			console.log("User@new = " + post);
		});*/
	},
	delCrew: function(crewId, userId) {
		/*Crews.find({}).forEach(function(post) {
			console.log("Crewb4del = " + post);
		});
		Meteor.users.find({}).forEach(function(post) {
			console.log("Userb4del = " + post);
		});*/


		Crews.remove(crewId, function(error) {
			console.log(error);
		});
		Meteor.users.update(userId, {$set:{"profile.crews":""}}, function(error, updated) {
			//TODO make it better
			console.log(error);
			console.log(updated);
		});


		/*Crews.find({}).forEach(function(post) {
			console.log("Crew@del = " + post);
		});
		Meteor.users.find({}).forEach(function(post) {
			console.log("User@del = " + post);
		});*/
	}
});