Notifications = new Meteor.Collection('notifications');

Notifications.allow({
  update: ownsDocument
});

createCommentNotification = function(comment) {
  var post = Posts.findOne(comment.postId);
  if (comment.userId !== post.userId) {
    Notifications.insert({
      userId: post.userId,
      postId: post._id,
      commentId: comment._id,
      commenterName: comment.author,
      read: false,
      adderId: null,
      adderName: null,
      crewId: null
    });
  }
};

createAddedToCrewNotification = function(adderID, addedId) {
  var adder = Meteor.users.findOne(adderID);
  var res = {
    userId: addedId,
    adderId: adderID,
    adderName: adder.profile.psuedo ? adder.profile.psuedo 
    : adder.profile.name,
    crewId: adder.profile.crews,
    read: false,
    postId: null,
    commentId: null,
    commenterName: null
  };

};