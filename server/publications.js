Meteor.publish('posts', function(options) {
  return Posts.find({}, options);//TODO add checks
});

Meteor.publish('singlePost', function(id) {
  return id && Posts.find(id);
});

Meteor.publish('comments', function(postId) {
  return Comments.find({postId: postId});
});

Meteor.publish('notifications', function() {
  return Notifications.find({userId: this.userId});
});

Meteor.publish("crews", function() {
	return Crews.find({members: this.userId});
});

Meteor.publish("crewMembers", function() {
	return Meteor.users.find({}); //TODO limit this
});

Meteor.publish("singleUser", function(id) {
	return id && Meteor.users.find(id);
});

Meteor.publish("postsOfUser", function(id) {
   return id && Posts.find({});//TODO fix
})